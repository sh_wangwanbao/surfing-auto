# 背景
当前流行的微服务架构，如采用Spring Cloud Alibaba技术栈，通常集成了Nacos作为服务注册和配置中心，Spring Gateway作为API网关，并通过Dubbo或OpenFeign实现不同Spring Boot业务模块间的通信。此外，该架构还融合了链路追踪、统一日志收集、分布式事务处理以及服务熔断和降级等高级功能。这些功能通过网关实现了统一的流量控制和身份验证，将分散的子系统整合为一个协同工作的整体，从而为业务运营提供了强大的技术支撑。然而，这种协同通常建立在统一的技术栈上——特别是Java作为主要的后端开发语言。

然而，在许多公司，尤其是那些有着多年技术积累的公司，技术栈往往是异构的，Java可能是主要语言，但Python和Go等语言也会用作支持。在这种情况下，如何使这些异构系统融入微服务生态，继续享受微服务带来的种种优势并支持业务的发展，成为了一个值得探讨的问题。

## 服务网格化的解决方案
服务网格化是一种网络通信的结构，专为解决微服务架构下服务通信的复杂性而设计。在这样的架构中，服务网格实现服务间的透明通讯、控制和监控，而无需更改服务本身的代码。它提供了一种方式来统一处理服务的发现、连接、负载均衡、故障转移、遥测数据收集、性能管理以及安全控制。

服务网格通常通过在服务实例旁部署的轻量级代理（如sidecar模式）来实现，代理拦截进出服务的流量，并按照配置的策略处理这些流量。服务网格的控制平面负责配置这些代理，并提供政策、服务发现和其他必要的信息。

而Istio是目前业界广泛认可的服务网格实现之一，提供了一整套解决方案，它被设计来处理服务发现、负载均衡、流量管理、遥测收集、安全保障等功能。

Istio的优点有：

- **遥测和监控：** Istio提供了详尽的监控、日志和追踪功能。
- **流量管理：**  灵活的路由规则、重试、故障注入和故障恢复。
- **安全性：**   自动化的认证和授权机制，加强服务间的安全性。

然而，Istio也存在着一些挑战：

- **资源要求：** Istio需要额外的资源来运行它的控制平面和代理。
- **学习曲线：** 功能丰富但复杂，新用户可能需要时间来学习和适应。
- **操作复杂度：** Istio的配置和维护比较复杂，对操作团队的技能要求较高。

## Surfing-Auto: 异构微服务集成解决方案

Surfing-Auto是一款专为打通异构系统间融合问题而设计的工具，它尤其适合于在不改变现有老系统架构的前提下，实现与新系统的集成。此方案突出了与Nacos服务治理的融合，实现了服务注册与发现机制。通过使用Python服务和REST API进行服务注册，以及通过发送心跳包维持服务活性，Surfing-Auto能够以较低的性能成本，实现服务集成和运维管理。

将这种机制集成到Spring Cloud Alibaba微服务框架中，Surfing-Auto能提供一个轻量级且高度可控的服务网格解决方案，这对于需要快速整合新旧系统的企业来说，是一个非常有吸引力的选择。

### 主要特点

- **轻量级设计：** Surfing-Auto采用非侵入式设计，保护现有系统免于大规模改动。

- **服务注册与发现：** 通过REST API轻松实现服务的注册与心跳检测，与Nacos服务治理无缝接轨。

- **性能优化：** 避免引入重型中间件，减少资源消耗和系统延迟。

- **Spring Cloud Alibaba集成：** 与Java微服务框架集成，扩展服务支持范围。

- **高度可控：** 简化的服务治理机制，便于系统监控和维护。



# Surfing-Auto项目愿景
>
> **Surfing-Auto** 灵感来源于冲浪者（Surfing）驾驭波浪的敏捷与变形金刚（Auto）的强大变换能力。这一命名象征着Surfing-Auto在微服务领域的高度适应性和可塑性。就像冲浪者巧妙地驾驭每一股波浪，Surfing-Auto能够灵活应对复杂多变的技术需求。
>
>**Surfing-Auto** 将Python与Java的微服务巧妙集成，利用两种语言各自的优势，打造出一个强大而灵活的分布式系统。通过py_bumblebee（大黄蜂）和java_optimus（擎天柱）两个子服务，Surfing-Auto不仅优化了性能，而且提高了系统的可扩展性。选择Surfing-Auto，意味着赋予你的项目以冲浪般的灵活性和变形金刚般的强悍力量，在快速演进的数字时代中，让我们的项目勇往直前，无所不能。
>

## Surfing-auto典型示例时序图
![Surfing-Auto Logo](Surfing_Auto.png)


## 模块介绍
* auto-serve : auto项目父模块，是一个pom工程
  * auto-authority :  auto项目的授权认证模块
  * auto-gateway: auto项目网关模块
  * java-optimus: auto项目中的异构java微服务项目父模块，是一个pom工程
    * order-server: 订单项目
  * py_bumblebee：auto项目中的异构python项目，是python微服务的父工程项目
    * product-server: 产品项目

* zeus-kernel：auto项目依赖于zeus项目的核心模块，使用多个starter加速开发



# 网关与授权中心相关接口
## 登录与认证相关
* 启动auto-gateway项目与auto-authority项目
* 通过接口访问创建用户


```
一个完整的请求逻辑与示例
通过网关访问java服务，读取产品信息+调用python服务分析
1. postman向网关发送请求数据，携带用户token。
2. 网关验证token成功。
3. 网关识别路径为order-server，转向order-server服务。
4. order-server根据order/findOrderProductById找到对应的controller进行处理。
5. order-server根据id，找到对应的定单基本信息。
6. order-server需要补全商品名称。
   6.1 在order表中，只有product_id。
   6.2 order-server服务根据配置的nacos找到gateway服务地址。
   6.3 拼装http协议 + 网关地址 + 网关端口 + 产品服务名 + 产品服务路径 + 产品参数，形成完整的请求路径。
   6.4 携带token调用resttemplate向网关发起请求，查询product-server商品信息。
   6.5 得到product-server商品信息，拼装成商品名称。
7. 完成产品详细信息，返回报文。

```


## 文档
[数据库脚本](./doc/Surfing-Auto.sql)
<br>
[postman代码](./doc/Surfing-Auto.postman_collection.json)


