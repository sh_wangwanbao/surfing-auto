package com.talang.surfing.auto.gateway.filter;

import com.talang.surfing.auto.gateway.entity.AutoGatewayProperties;
import com.tdh.zeus.core.common.constant.ZeusConstant;
import com.tdh.zeus.core.common.enums.HeaderEnum;
import com.tdh.zeus.core.common.util.$;
import com.tdh.zeus.core.common.util.JwtUtil;
import com.tdh.zeus.core.common.util.ResponseUtil;
import com.tdh.zeus.core.common.util.StringPool;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 走sso登录，在微服务应用中配置，网关中不再做鉴权验证
 * 如果需要生效，则开启@Component注解
 *
 * @author wangwanbao
 */
@Slf4j
@Component
@AllArgsConstructor
public class PreUaaFilter implements GlobalFilter, Ordered {

    private final AutoGatewayProperties autoGatewayProperties;

    /**
     * 路径前缀以/mate开头，如mate-system
     */
    private static final String[] PATH_PREFIXS = new String[]{"/utruck-front", "/kh-front", "/auto-authority"};

    /**
     * 忽略路径path
     */
    private static final String[] IGNORE_PATH = new String[]{"/getSourceHallInfoDetails"};

    /**
     * 索引自1开头检索，跳过第一个字符就是检索的字符的问题
     */
    public static final int FROM_INDEX = 1;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //　如果在忽略的url里，则跳过
        String path = replacePrefix(exchange.getRequest().getURI().getPath());
        String requestUrl = exchange.getRequest().getURI().getRawPath();
        log.info("path: {}, requestUrl: {}", path, requestUrl);
        if (ignore(path) || ignore(requestUrl) || ignorePath(path)) {
            return chain.filter(exchange);
        }
        // 验证token是否有效
        ServerHttpResponse resp = exchange.getResponse();
        String headerToken = exchange.getRequest().getHeaders().getFirst(HeaderEnum.AuthToken.getValue());
        String mobile = JwtUtil.parseToken(headerToken, String.class);
        if($.isBlank(mobile)) {
            return unauthorized(resp, "token认证失败！");
        }
        return chain.filter(exchange);
    }

    /**
     * 检查是否忽略url
     *
     * @param path 路径
     * @return boolean
     */
    private boolean ignore(String path) {
        return autoGatewayProperties.getIgnoreUrl().stream()
                .map(url -> url.replace("/**", ""))
                .anyMatch(path::startsWith);
    }

    /**
     * 检查是否忽略path
     *
     * @param path 路径
     * @return boolean
     */
    private boolean ignorePath(String path) {
        for (String ignorePath : IGNORE_PATH) {
            if (path.contains(ignorePath)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 移除模块前缀
     *
     * @param path 路径
     * @return String
     */
    private String replacePrefix(String path) {
        for (String pathPrefix : PATH_PREFIXS) {
            if (path.startsWith(pathPrefix)) {
                return path.substring(path.indexOf(StringPool.SLASH, FROM_INDEX));
            }
        }
        return path;
    }

    private Mono<Void> unauthorized(ServerHttpResponse resp, String msg) {
        return ResponseUtil.webFluxResponseWriter(resp, ZeusConstant.JSON_UTF8, HttpStatus.UNAUTHORIZED, msg);
    }

    @Override
    public int getOrder() {
        return ZeusConstant.MATE_UAA_FILTER_ORDER;
    }

}
