package com.talang.surfing.auto.gateway.handler;

import com.talang.surfing.auto.gateway.entity.AutoGatewayProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * 网关默认首页
 *
 * @author pangu
 */
@RestController
public class IndexHandler {
    
    @Autowired
    private AutoGatewayProperties autoGatewayProperties;

    @GetMapping("/")
    public Mono<String> index() {
        return Mono.just(desc());
    }

    private String desc() {
        StringBuilder sb = new StringBuilder(100);
        sb.append("<div style='color: blue'>Zeus gateway has been started!</div>");
        sb.append("<br/>");
        sb.append("<div><ul><li>文档地址：<a href='doc.html'>doc.html</a></li></ul></div>");
        return sb.toString();
    }
}
