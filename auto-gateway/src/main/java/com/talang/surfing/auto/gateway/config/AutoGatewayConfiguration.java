package com.talang.surfing.auto.gateway.config;

import com.talang.surfing.auto.gateway.entity.AutoGatewayProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/15 1:54 PM
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({AutoGatewayProperties.class})
public class AutoGatewayConfiguration {
    
}
