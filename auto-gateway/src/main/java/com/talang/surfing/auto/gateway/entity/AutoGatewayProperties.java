package com.talang.surfing.auto.gateway.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/15 1:55 PM
 */
@Data
@ConfigurationProperties()
@Slf4j
public class AutoGatewayProperties {
    /**
     * 忽略URL，List列表形式
     */
    @Value("${auto.gateway.ignore-url:}")
    private List<String> ignoreUrl;
    
}
