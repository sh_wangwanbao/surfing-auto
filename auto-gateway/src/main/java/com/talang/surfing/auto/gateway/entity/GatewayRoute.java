package com.talang.surfing.auto.gateway.entity;

import lombok.Data;
import org.springframework.cloud.gateway.route.RouteDefinition;

import java.io.Serializable;
import java.util.List;

/**
 * 网关路由实体类
 *
 * @author pangu
 */
@Data
public class GatewayRoute implements Serializable {
	List<RouteDefinition> routes;
}
