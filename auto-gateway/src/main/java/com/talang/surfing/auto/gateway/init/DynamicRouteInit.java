package com.talang.surfing.auto.gateway.init;

import com.alibaba.cloud.nacos.NacosConfigProperties;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.talang.surfing.auto.gateway.entity.GatewayRoute;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.route.RouteDefinitionWriter;
import org.springframework.context.annotation.Configuration;
import org.yaml.snakeyaml.Yaml;
import reactor.core.publisher.Mono;
import com.tdh.zeus.core.common.constant.ZeusConstant;

import javax.annotation.PostConstruct;
import java.util.Properties;
import java.util.concurrent.Executor;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class DynamicRouteInit {

	private final RouteDefinitionWriter routeDefinitionWriter;
	private final NacosConfigProperties nacosProperties;

	@PostConstruct
	public void initRoute() {
		try {
			Properties properties = new Properties();
			properties.put(PropertyKeyConst.SERVER_ADDR, nacosProperties.getServerAddr());
			properties.put(PropertyKeyConst.USERNAME, nacosProperties.getUsername());
			properties.put(PropertyKeyConst.PASSWORD, nacosProperties.getPassword());
			properties.put(PropertyKeyConst.NAMESPACE, nacosProperties.getNamespace());
			ConfigService configService = NacosFactory.createConfigService(properties);

			String content = configService.getConfig(ZeusConstant.NACOS_ROUTES_NAME, nacosProperties.getGroup(), ZeusConstant.NACOS_ROUTES_TIMEOUT);
			log.info(":cloud: init gateway start");
			updateRoute(content);
			log.info(":cloud: init gateway end");
			//开户监听，实现动态
			configService.addListener(ZeusConstant.NACOS_ROUTES_NAME, nacosProperties.getGroup(), new Listener() {
				@Override
				public void receiveConfigInfo(String configInfo) {
					log.info(":cloud: update gateway start");
					updateRoute(configInfo);
					log.info(":cloud: update gateway end");
				}
				@Override
				public Executor getExecutor() {
					return null;
				}
			});
		} catch (NacosException e) {
			log.error("load routes error：{}", e.getErrMsg());
		}
	}

	public void updateRoute(String content) {
		Yaml yaml = new Yaml();
		GatewayRoute gatewayRoute = yaml.loadAs(content, GatewayRoute.class);
		gatewayRoute.getRoutes().forEach(route -> {
			log.info("load routes：{},{}", route.getId(), route);
			routeDefinitionWriter.save(Mono.just(route)).subscribe();
		});
	}
}
