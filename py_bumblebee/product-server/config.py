#! /usr/bin/env python
# @Time:  
# @Author: wangwanbao
# -*- coding: utf-8 -*-

# 服务名称
server_name = "product-server"

# 服务端口
server_port = 50004

#nacos主机

nacos_host = "192.168.69.141"

# nacos端口
nacos_port = 8848