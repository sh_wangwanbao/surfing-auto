#! /usr/bin/env python
# @Time:  
# @Author: wangwanbao
# -*- coding: utf-8 -*-
import jwt
import json
from datetime import datetime
import base64

PUBLIC_KEY = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv+jnLF5AJeGR8Qc4EPegkHDEM0QZxwpjEl6TwpG4HZhXz3ES2pCDseY9xhOhgUf1LfhBJybeLniFAR2Y
N13PUlruwQozXsloUdz8PRiZxSeKHYllNgoyyjAHKUviL41G7N0GQDOGDPv59Z3I7bJv0P2hQ7ytTeQ/UgGoQNsR5cd4Ve9CIwEASESbsTXsIV8n/ToLukM0JdXh+FxEaFMzqiEJSSWxwDoFSUfH4/xKi3vQhDC
KatbTZNs1EqHinKRGLBD4EWWE4yGWafBUxq5T0ygIymT1CBEAMSpb728fcOuGxe57DedDngoDQvXI9fc2otzQ22RAhXBqGXXgiTkjgwIDAQAB
-----END PUBLIC KEY-----"""

def parse_token(token: str) -> dict:
    try:
        if not token:
            return None

        claims = jwt.decode(token, PUBLIC_KEY, algorithms=["RS256"])

        # 如果 Token 已经过期了, 返回 None
        if 'exp' in claims and datetime.utcfromtimestamp(claims['exp']) < datetime.utcnow():
            return None

        print(claims)
        # 返回 Token 中保存的用户信息
        return json.loads(claims['tdh-user'])
    except jwt.ExpiredSignatureError:
        # 超时异常处理
        return None

if __name__ == "__main__":
    token = "eyJhbGciOiJSUzI1NiJ9.eyJ0ZGgtdXNlciI6IlwiMTg1MjEzMTYwNzdcIiIsImp0aSI6ImY1OWNkY2UxLWM2NTItNGU5Ny05ZDY4LTIyNDE2YjkxZDBkNSIsImV4cCI6MTY4MjU2NTY4NH0.huV0damYooOgKeVObzlrXoyy7iJIYDWgboBX4v-HSViXvYCGEETQKth9a7taWP1SWW27XmcZbBkRhHCUrYj9CYiAkf75VojnaI_HBxYF1aHp-cJCaHEhqdiPpcxul8VavHM8Ok1Howa_ib7awX7VnHEd18l4ma-fsEeajMoazeIQkj0H3vPlVM3_ymRwF9nwyST8jmpocbuvMFm2wAHJvThfLoI07XL1vmaxd0PBj2qmHXEe9GSijlFbBmObTz1WKOOhqhiTWj6s0qRS3yUVzHJhHZvwfU0iGh85a2pp504026wMgQAvhEzId9ml3VIz1tmyBSetZiaxCa9wxlNo7Q"
    user_info = parse_token(token)
    print(user_info)