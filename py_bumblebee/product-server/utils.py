#! /usr/bin/env python
# @Time:  
# @Author: wangwanbao
# -*- coding: utf-8 -*-
# utils.py
import socket

class Util:
    @staticmethod
    def get_lan_ip():
        hostname = socket.gethostname()
        addr_info_list = socket.getaddrinfo(hostname, None, family=socket.AF_INET)

        for addr_info in addr_info_list:
            ip = addr_info[4][0]
            if ip.startswith("192.168.") or ip.startswith("10.") or (
                ip.startswith("172.") and 16 <= int(ip.split(".")[1]) <= 31
            ):
                return ip

        return None
