#! /usr/bin/env python
# @Time:
# @Author: wangwanbao
# -*- coding: utf-8 -*-
import requests
import threading
import time
import config
from flask import Flask,jsonify,request
server = Flask(__name__)
server.config['JSON_AS_ASCII']=False
from utils import Util
import mysql.connector

@server.route('/greet',methods=['get'])
def greet():
    # 处理业务逻辑
    name = request.args['name']
    result = \
        {"code":"200",
         "msg":"",
         "data":{"name":name,"age":25,"job":"python"},
         "success" : True
         }
    return jsonify(result)

# 连接到MySQL数据库
def create_conn():
    conn = mysql.connector.connect(
        host="192.168.69.141",
        user="zeus",
        password="XEbs&EqtFk3EhMt&",
        database="auto_serve"
    )
    return conn

# 根据ID查询auto_product表
@server.route('/product/<int:product_id>', methods=['GET'])
def get_product_by_id(product_id):
    conn = create_conn()
    cursor = conn.cursor()

    query = "SELECT id, name FROM auto_product WHERE id = %s"
    cursor.execute(query, (product_id,))
    result = cursor.fetchone()

    if result:
        product = {"id": result[0], "name": result[1]}
        return jsonify(product)
    else:
        return jsonify({"error": "Product not found"}), 404

#nacos服务
def service_register():
    # url = "http://192.168.65.16:8848/nacos/v1/ns/instance?serviceName=product-server&ip=192.168.65.16&port=8085"
    url_template = "http://{}:{}/nacos/v1/ns/instance?serviceName=product-server&ip={}&port={}"
    res = requests.post(url_template.format(config.nacos_host, config.nacos_port, config.node_host, config.server_port))
    print("向nacos注册中心，发起服务注册请求，注册响应状态： {}".format(res.status_code))

#服务检测
def service_beat():
    while True:
        # url_template = "http://192.168.65.16:8848/nacos/v1/ns/instance/beat?serviceName=product-server&ip=192.168.65.16&port=8085"
        url_template = "http://{}:{}/nacos/v1/ns/instance/beat?serviceName={}&ip={}&port={}"
        res = requests.put(url_template.format(config.nacos_host, config.nacos_port, config.server_name, config.node_host, config.server_port))
        print("已注册服务，执行心跳服务，续期服务响应状态： {}".format(res.status_code))
        time.sleep(5)


if __name__ == "__main__":
    config.node_host = Util.get_lan_ip()
    # print(config.server_name)
    service_register()
    # 5秒以后，异步执行service_beat()方法
    threading.Timer(5, service_beat).start()
    server.run(host='0.0.0.0', port=config.server_port, debug=True)
