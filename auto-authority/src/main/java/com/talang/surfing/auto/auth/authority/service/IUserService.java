package com.talang.surfing.auto.auth.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.talang.surfing.auto.auth.authority.entity.User;
import com.talang.surfing.auto.auth.authority.qo.UserSaveQO;
import com.tdh.zeus.core.common.api.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-18
 */
public interface IUserService extends IService<User> {

    /**
     * 创建用户
     * @param userSaveQO
     * @return
     */
    Result<?> saveUser(UserSaveQO userSaveQO);

    
}
