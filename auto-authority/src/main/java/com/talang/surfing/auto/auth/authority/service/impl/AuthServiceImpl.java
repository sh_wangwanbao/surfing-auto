package com.talang.surfing.auto.auth.authority.service.impl;

import cn.hutool.crypto.digest.BCrypt;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.talang.surfing.auto.auth.authority.service.IAuthService;
import com.talang.surfing.auto.auth.authority.service.IUserService;
import com.talang.surfing.auto.auth.authority.entity.User;
import com.talang.surfing.auto.auth.authority.qo.LoginQO;
import com.talang.surfing.auto.auth.authority.util.Constant;
import com.talang.surfing.auto.auth.authority.vo.LoginVO;
import com.tdh.zeus.core.common.util.DateUtil;
import com.tdh.zeus.core.common.util.JwtUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

/**
 * @Description:13335628666
 * @author: wangwanbao
 * @Date: 2023/4/19 9:57 AM
 */
@Service
public class AuthServiceImpl implements IAuthService {

    @Resource
    private IUserService userService;

    @Override
    public LoginVO login(LoginQO loginQO) {
        User dbUser = userService.getOne(new LambdaQueryWrapper<User>().eq(User::getMobile, loginQO.getMobile()));
        //输入不存在的手机号
        if (Objects.isNull(dbUser)) {
            return new LoginVO().setLoginStatus(false).setMessage(String.format("手机号%s不存在", loginQO.getMobile()));
        }
        boolean isPasswordValid = BCrypt.checkpw(loginQO.getPassword(), dbUser.getPassword());
        if (isPasswordValid == false) {
            return new LoginVO().setLoginStatus(false).setMessage("密码不匹配");
        }
        Date expireDate = DateUtil.plusHours(new Date(), Constant.TOKEN_VALID_HOURS);
        String accessToken = JwtUtil.generateToken(loginQO.getMobile(), expireDate);
        return new LoginVO().setLoginStatus(true).setAccessToken(accessToken);
    }

}
