package com.talang.surfing.auto.auth.authority.service.impl;

import cn.hutool.crypto.digest.BCrypt;
import com.talang.surfing.auto.auth.authority.service.IUserService;
import com.talang.surfing.auto.auth.authority.entity.User;
import com.talang.surfing.auto.auth.authority.mapper.UserMapper;
import com.talang.surfing.auto.auth.authority.qo.UserSaveQO;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tdh.zeus.core.common.api.Result;
import com.tdh.zeus.core.id.generator.wroker.IdWorkerUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-18
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public Result<?> saveUser(UserSaveQO userSaveQO) {
        User userEntity = new User();
        Long id = IdWorkerUtil.nextId();
        userEntity.setMobile(userSaveQO.getMobile());
        //创建bcrypt加密密码
        String hashedPassword = BCrypt.hashpw(userSaveQO.getPassword(), BCrypt.gensalt());
        userEntity.setPassword(hashedPassword);
        userEntity.setId(id);
        boolean result = this.save(userEntity);
        return result ? Result.data("创建成功") : Result.fail("创建失败");
    }
}
