package com.talang.surfing.auto.auth.authority.mapper;

import com.talang.surfing.auto.auth.authority.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-18
 */
public interface UserMapper extends BaseMapper<User> {

}
