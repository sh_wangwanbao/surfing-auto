package com.talang.surfing.auto.auth.authority.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.talang.surfing.auto.auth.authority.service.IUserService;
import com.talang.surfing.auto.auth.authority.vo.UserVO;
import com.talang.surfing.auto.auth.authority.entity.User;
import com.talang.surfing.auto.auth.authority.qo.UserSaveQO;
import com.tdh.zeus.core.common.api.Result;
import com.tdh.zeus.core.common.util.$;
import com.tdh.zeus.core.validate.annations.ZeusValidate;
import com.tdh.zeus.core.validate.enums.Check;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-18
 */
@RestController
@RequestMapping("/user")
@Api(value = "用户接口", tags = "用户接口")
@Slf4j
public class UserController {

    @Resource
    private IUserService userService;

    @PostMapping("/saveUser")
    @ApiOperation(value = "用户管理-创建用户", notes = "创建用户接口")
    @ApiImplicitParam(value = "用户管理-创建用户", paramType = "form")
    public Result<?> saveUser(@Valid @RequestBody UserSaveQO userSaveQO) {
        return userService.saveUser(userSaveQO);
    }

    @GetMapping("/findUserByMobile")
    @ApiOperation(value = "用户管理-查找用户", notes = "用户管理-查找用户")
    @ApiImplicitParam(value = "用户管理-根据手机查询用户", paramType = "json")
    public Result<?> findUserByMobile(@ZeusValidate(fun = Check.MobilePhone) @RequestParam("mobile") String mobile) {
        User user = userService.getOne((new LambdaQueryWrapper<User>().eq(User::getMobile, mobile))
                .and(wrapper -> wrapper.eq(User::getIsDeleted, 0)));
        if (Objects.nonNull(user)) {
            UserVO userVO = $.copyWithConvert(user, UserVO.class);
            return Result.data(userVO);
        } else {
            return Result.data("查无用户");
        }
    }

}
