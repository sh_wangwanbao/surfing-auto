package com.talang.surfing.auto.auth.authority;

import com.tdh.zeus.core.feign.annotation.EnableMateFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/17 10:08 AM
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableMateFeign
@ComponentScans(value = {
        @ComponentScan(value = "com.tdh")
})
public class AutoAuthorityServer {
    public static void main(String[] args) {
        SpringApplication.run(AutoAuthorityServer.class, args);
    }
}
