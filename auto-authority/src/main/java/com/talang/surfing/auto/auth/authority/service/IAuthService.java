package com.talang.surfing.auto.auth.authority.service;

import com.talang.surfing.auto.auth.authority.qo.LoginQO;
import com.talang.surfing.auto.auth.authority.vo.LoginVO;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/19 9:56 AM
 */
public interface IAuthService {
    /**
     * 用户登录
     * @param loginQO
     * @return
     */
    LoginVO login(LoginQO loginQO);
    
}
