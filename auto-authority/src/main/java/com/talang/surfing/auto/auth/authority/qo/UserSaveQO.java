package com.talang.surfing.auto.auth.authority.qo;

import com.tdh.zeus.core.validate.annations.ZeusValidate;
import com.tdh.zeus.core.validate.enums.Check;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/18 4:25 PM
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "UserCreateQO", description = "创建用户信息")
public class UserSaveQO implements Serializable {

    @ApiModelProperty("手机号")
    @ZeusValidate(required = true, fun = Check.MobilePhone)
    private String mobile;

    @ApiModelProperty("密码，使用Bcrypt加密，Bcrypt通常是60个字符，新版本可能是72个字符")
    @ZeusValidate(required = true, fun = Check.NotEmpty, message = "密码不能为空")
    private String password;

}
