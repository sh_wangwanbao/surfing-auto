package com.talang.surfing.auto.auth.authority.controller;

import cn.hutool.http.HttpStatus;
import com.talang.surfing.auto.auth.authority.qo.LoginQO;
import com.talang.surfing.auto.auth.authority.service.IAuthService;
import com.talang.surfing.auto.auth.authority.vo.LoginVO;
import com.tdh.zeus.core.common.api.Result;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/17 10:55 AM
 */
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {

    @Resource
    private IAuthService authService;

    @GetMapping("/hello")
    public Result<?> hello() {
        return Result.data("hello");
    }

    @PostMapping("/login")
    @ApiOperation(value = "授权管理-用户登录", notes = "用户登录接口")
    @ApiImplicitParam(value = "授权管理-用户登录", paramType = "json")
    public Result<String> login(@Valid @RequestBody LoginQO loginQO) {
        LoginVO loginVO = authService.login(loginQO);
        return loginVO.getLoginStatus() ? Result.data(loginVO.getAccessToken()) 
                : Result.fail(HttpStatus.HTTP_UNAUTHORIZED, loginVO.getMessage());
    }


}
