package com.talang.surfing.auto.auth.authority.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/19 9:48 AM
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "LoginVO", description = "用户登录返回")
public class LoginVO implements Serializable {

    /**
     * 登录状态
     */
    private Boolean loginStatus;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 登录token
     */
    private String accessToken;

}
