package com.talang.surfing.auto.auth.authority.util;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/19 10:14 AM
 */
public class Constant {
    
    //token时效：24小时
    public static final Integer TOKEN_VALID_HOURS = 24;
    
}
