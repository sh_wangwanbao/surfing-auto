package com.talang.surfing.auto.auth.authority.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/21 2:38 PM
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "UserVO", description = "用户信息返回")
public class UserVO implements Serializable {
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("创建人")
    private Long createBy;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新人")
    private Long updateBy;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("是否删除")
    private Integer isDeleted;

    @ApiModelProperty("版本")
    private Integer version;
}
