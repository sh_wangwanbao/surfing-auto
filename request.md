***
* 登录接口
```
POST /auto-authority/auth/login HTTP/1.1
Host: localhost:50001
Content-Type: application/json
Content-Length: 58

{
     "mobile": "18521316077",
    "password": "123456"
}
```
* 返回报文示例
  ```
{
"code": 200,
"msg": "处理成功",
"time": 1682501368275,
"success": true,
"userMsg": "处理成功",
"data": "eyJhbGciOiJSUzI1NiJ9.eyJ0ZGgtdXNlciI6IlwiMTg1MjEzMTYwNzdcIiIsImp0aSI6IjYxNzM1ODE5LWJlNGMtNDhlZi05YzA2LWZjYjAyZDMyOGNmNSIsImV4cCI6MTY4MjU4Nzc2N30.XVXZ0Yx73uO5mErSdPxcmLtpf-KbFgE-sOl2pt0QIQT6T0akJL8xHrAKmP-nugINQ83lWN0tpeoUy6paivFLmfq82XiQRHEJv5R6eiLEcByhmUSC8mWcj5N5BhPM3dCXhvDymxav-ZC_lYuG-tPW-Qs5uNELNr8ouGfaiGlQ2KSjP5QZRXWeibjQiQPiiReLUArYXL8XsbQL_QuQt8Fx8DNV6txIc0A9CebLb_H4xY6F6D6x_ixy8f2gj56cJkbGjSr8H-h09xSoXuCLZY1gIgiSI3WjpS_zuNfd9432A7Zxf8ZbZj2oTTYsxYYDkuyXlEk9FGJVBmcUerbWavsdpg"
}
  ```

***

* 访问授权中心hello接口(不需要token访问）
```
GET /auto-authority/auth/hello HTTP/1.1
Host: 192.168.65.16:50001
```

* 返回报文示例
```
{
"code": 200,
"msg": "处理成功",
"time": 1682501415140,
"success": true,
"userMsg": "处理成功",
"data": "hello"
}
```
***
* 访问授权中心用户接口(不带token访问，预期会失败)
```
GET /auto-authority/user/findUserByMobile?mobile=18521316077 HTTP/1.1
Host: 192.168.65.16:50001
```

* 返回报文
```
{
"code": 401,
"msg": "token认证失败！",
"success": false,
"time": 1682501495235
}
```

***

* 访问授权中心用户接口(带toen访问）

```
GET /auto-authority/user/findUserByMobile?mobile=18521316077 HTTP/1.1
Host: 192.168.65.16:50001
Auth-token: eyJhbGciOiJSUzI1NiJ9.eyJ0ZGgtdXNlciI6IlwiMTg1MjEzMTYwNzdcIiIsImp0aSI6ImY1OWNkY2UxLWM2NTItNGU5Ny05ZDY4LTIyNDE2YjkxZDBkNSIsImV4cCI6MTY4MjU2NTY4NH0.huV0damYooOgKeVObzlrXoyy7iJIYDWgboBX4v-HSViXvYCGEETQKth9a7taWP1SWW27XmcZbBkRhHCUrYj9CYiAkf75VojnaI_HBxYF1aHp-cJCaHEhqdiPpcxul8VavHM8Ok1Howa_ib7awX7VnHEd18l4ma-fsEeajMoazeIQkj0H3vPlVM3_ymRwF9nwyST8jmpocbuvMFm2wAHJvThfLoI07XL1vmaxd0PBj2qmHXEe9GSijlFbBmObTz1WKOOhqhiTWj6s0qRS3yUVzHJhHZvwfU0iGh85a2pp504026wMgQAvhEzId9ml3VIz1tmyBSetZiaxCa9wxlNo7Q
```

* 返回报文
```
{
"code": 200,
"msg": "处理成功",
"time": 1682501622179,
"success": true,
"userMsg": "处理成功",
"data": {
"id": "1166090226980880384",
"mobile": "18521316077",
"createBy": 0,
"createTime": "2023-04-19 10:41:10",
"updateBy": 0,
"updateTime": "2023-04-19 10:41:10",
"isDeleted": 0,
"version": 0
}
}
```

***
* 不通过网关java通过openfeign直连python

```
GET /order/greet?name=wwb HTTP/1.1
Host: 192.168.65.16:50003
```

* 返回报文

```
{
"code": 200,
"msg": "处理成功",
"time": 1682501721227,
"success": true,
"userMsg": "处理成功",
"data": {
"name": "wwb",
"job": "python",
"age": 25
}
}
```

***
* 通过网关访问java服务，读取定单信息

```
GET /order-server/order/findById?id=1167644817539203072 HTTP/1.1
Host: 192.168.65.16:50001
Auth-token: eyJhbGciOiJSUzI1NiJ9.eyJ0ZGgtdXNlciI6IlwiMTg1MjEzMTYwNzdcIiIsImp0aSI6ImY1OWNkY2UxLWM2NTItNGU5Ny05ZDY4LTIyNDE2YjkxZDBkNSIsImV4cCI6MTY4MjU2NTY4NH0.huV0damYooOgKeVObzlrXoyy7iJIYDWgboBX4v-HSViXvYCGEETQKth9a7taWP1SWW27XmcZbBkRhHCUrYj9CYiAkf75VojnaI_HBxYF1aHp-cJCaHEhqdiPpcxul8VavHM8Ok1Howa_ib7awX7VnHEd18l4ma-fsEeajMoazeIQkj0H3vPlVM3_ymRwF9nwyST8jmpocbuvMFm2wAHJvThfLoI07XL1vmaxd0PBj2qmHXEe9GSijlFbBmObTz1WKOOhqhiTWj6s0qRS3yUVzHJhHZvwfU0iGh85a2pp504026wMgQAvhEzId9ml3VIz1tmyBSetZiaxCa9wxlNo7Q
```

* 返回报文
```
{
"code": 200,
"msg": "处理成功",
"time": 1682501751845,
"success": true,
"userMsg": "处理成功",
"data": {
"id": "1167644817539203072",
"orderNo": "1167644793178685440",
"userId": "1166090226980880384",
"totalAmount": 300.12000,
"productId": 1,
"orderStatus": 0,
"createBy": 0,
"createTime": "2023-04-23 17:38:33",
"updateBy": 0,
"updateTime": "2023-04-23 17:38:34",
"isDeleted": 0,
"version": 0
}
}
```

***
* 通过网关访问python服务，读取产品信息
```
GET /product-server/product/1 HTTP/1.1
Host: 192.168.65.16:50001
Auth-token: eyJhbGciOiJSUzI1NiJ9.eyJ0ZGgtdXNlciI6IlwiMTg1MjEzMTYwNzdcIiIsImp0aSI6ImY1OWNkY2UxLWM2NTItNGU5Ny05ZDY4LTIyNDE2YjkxZDBkNSIsImV4cCI6MTY4MjU2NTY4NH0.huV0damYooOgKeVObzlrXoyy7iJIYDWgboBX4v-HSViXvYCGEETQKth9a7taWP1SWW27XmcZbBkRhHCUrYj9CYiAkf75VojnaI_HBxYF1aHp-cJCaHEhqdiPpcxul8VavHM8Ok1Howa_ib7awX7VnHEd18l4ma-fsEeajMoazeIQkj0H3vPlVM3_ymRwF9nwyST8jmpocbuvMFm2wAHJvThfLoI07XL1vmaxd0PBj2qmHXEe9GSijlFbBmObTz1WKOOhqhiTWj6s0qRS3yUVzHJhHZvwfU0iGh85a2pp504026wMgQAvhEzId9ml3VIz1tmyBSetZiaxCa9wxlNo7Q
```

* 返回报文
```
{
"id": 1,
"name": " Apple iPhone 14 Pro (A2892) 256GB 银色 支持移动联通电信5G 双卡双待手机"
}
```


* 通过网关访问java服务，读取产品信息+调用python服务

```
GET /order-server/order/findOrderProductById?id=1167644817539203072 HTTP/1.1
Host: 192.168.65.16:50001
Auth-token: eyJhbGciOiJSUzI1NiJ9.eyJ0ZGgtdXNlciI6IlwiMTg1MjEzMTYwNzdcIiIsImp0aSI6ImY1OWNkY2UxLWM2NTItNGU5Ny05ZDY4LTIyNDE2YjkxZDBkNSIsImV4cCI6MTY4MjU2NTY4NH0.huV0damYooOgKeVObzlrXoyy7iJIYDWgboBX4v-HSViXvYCGEETQKth9a7taWP1SWW27XmcZbBkRhHCUrYj9CYiAkf75VojnaI_HBxYF1aHp-cJCaHEhqdiPpcxul8VavHM8Ok1Howa_ib7awX7VnHEd18l4ma-fsEeajMoazeIQkj0H3vPlVM3_ymRwF9nwyST8jmpocbuvMFm2wAHJvThfLoI07XL1vmaxd0PBj2qmHXEe9GSijlFbBmObTz1WKOOhqhiTWj6s0qRS3yUVzHJhHZvwfU0iGh85a2pp504026wMgQAvhEzId9ml3VIz1tmyBSetZiaxCa9wxlNo7Q
```

* 返回报文

{
"code": 200,
"msg": "处理成功",
"time": 1682501904232,
"success": true,
"userMsg": "处理成功",
"data": {
"id": "1167644817539203072",
"orderNo": "1167644793178685440",
"userId": "1166090226980880384",
"totalAmount": 300.12000,
"productId": 1,
"productName": " Apple iPhone 14 Pro (A2892) 256GB 银色 支持移动联通电信5G 双卡双待手机",
"orderStatus": 0,
"createBy": 0,
"createTime": "2023-04-23 17:38:33",
"updateBy": 0,
"updateTime": "2023-04-23 17:38:34",
"isDeleted": 0,
"version": 0
}
}
