CREATE DATABASE `auto_serve` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;

use `auto_serve`;

CREATE TABLE `auth_user` (
                             `id` bigint NOT NULL COMMENT '主键',
                             `mobile` varchar(100) COLLATE utf8mb4_bin NOT NULL COMMENT '手机号',
                             `password` varchar(72) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码，使用Bcrypt加密，Bcrypt通常是60个字符，新版本可能是72个字符',
                             `create_by` bigint DEFAULT '0' COMMENT '创建人',
                             `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                             `update_by` bigint DEFAULT '0' COMMENT '更新人',
                             `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                             `is_deleted` tinyint NOT NULL DEFAULT '0' COMMENT '是否删除',
                             `version` int DEFAULT '0' COMMENT '版本',
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `auto_order` (
                              `id` bigint NOT NULL COMMENT '主键',
                              `order_no` bigint NOT NULL COMMENT '订单单号',
                              `user_id` bigint NOT NULL COMMENT '用户id',
                              `total_amount` decimal(18,5) NOT NULL DEFAULT '0.00000' COMMENT '订单金额',
                              `product_id` bigint NOT NULL COMMENT '产品id',
                              `order_status` int NOT NULL COMMENT '订单状态',
                              `create_by` bigint DEFAULT '0' COMMENT '创建人',
                              `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                              `update_by` bigint DEFAULT '0' COMMENT '更新人',
                              `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                              `is_deleted` tinyint NOT NULL DEFAULT '0' COMMENT '是否删除',
                              `version` int DEFAULT '0' COMMENT '版本',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `auto_product` (
                                `id` bigint NOT NULL COMMENT '主键',
                                `name` varchar(100) COLLATE utf8mb4_bin NOT NULL COMMENT '产品名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


INSERT INTO auto_serve.auth_user (id,mobile,password,create_by,create_time,update_by,update_time,is_deleted,version) VALUES
    (1166090226980880384,'18521316077','$2a$10$/fbLR5u20EjFQ4tDsPgBfeorSoEuf7Jd0Ufiq4n3oEemBzpqDf2hG',0,'2023-04-19 10:41:10',0,'2023-04-19 10:41:10',0,0);
INSERT INTO auto_serve.auto_order (id,order_no,user_id,total_amount,product_id,order_status,create_by,create_time,update_by,update_time,is_deleted,version) VALUES
    (1167644817539203072,1167644793178685440,1166090226980880384,300.12000,1,0,0,'2023-04-23 17:38:33',0,'2023-04-23 17:38:34',0,0);
INSERT INTO auto_serve.auto_product (id,name) VALUES
    (1,' Apple iPhone 14 Pro (A2892) 256GB 银色 支持移动联通电信5G 双卡双待手机');