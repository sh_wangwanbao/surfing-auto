package com.talang.surfing.auto.order.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.google.common.collect.ImmutableMap;
import com.talang.surfing.auto.common.annotations.EnableAutoSubject;
import com.talang.surfing.auto.common.model.AutoSubject;
import com.talang.surfing.auto.common.util.Constant;
import com.talang.surfing.auto.common.util.NacosUtil;
import com.talang.surfing.auto.common.util.PathUtil;
import com.talang.surfing.auto.order.service.IOrderService;
import com.talang.surfing.auto.order.vo.OrderProductVO;
import com.talang.surfing.auto.order.vo.OrderVO;
import com.talang.surfing.auto.order.provider.IProductProvider;
import com.talang.surfing.auto.order.qo.OrderSaveQO;
import com.tdh.zeus.core.cloud.util.RestUtil;
import com.tdh.zeus.core.common.api.Result;
import com.tdh.zeus.core.common.enums.HeaderEnum;
import com.tdh.zeus.core.common.util.$;
import com.tdh.zeus.core.common.util.RequestHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-23
 */
@RestController
@RequestMapping("/order")
@Api(value = "订单接口", tags = "订单接口")
@Slf4j
public class OrderController {

    @Resource
    private IOrderService orderService;

    @Resource
    private IProductProvider productProvider;

    @Resource
    private RestUtil restUtil;

    @Resource
    private NacosServiceManager nacosServiceManager;

    @Resource
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    @PostMapping("/saveOrder")
    @ApiOperation(value = "订单管理-创建订单", notes = "创建订单接口")
    @ApiImplicitParam(value = "用户管理-创建用户", paramType = "json")
    public Result<?> saveOrder(@Valid @RequestBody OrderSaveQO orderSaveQO) {
        boolean successful = orderService.saveOrder(orderSaveQO);
        return successful ? Result.data("成功") : Result.fail(500, "创建用户失败");
    }


    @GetMapping("/testNacos")
    public Result<?> test() {
        try {
            List<Instance> allInstances = nacosServiceManager.getNamingService(nacosDiscoveryProperties.getNacosProperties()).getAllInstances(Constant.AUTO_GATEWAY_SERVER_NAME);
            allInstances.stream().forEach(instance -> {
                System.out.println(instance.getIp() + " " + instance.getPort());
            });
        } catch (NacosException e) {
            throw new RuntimeException(e);
        }
        return Result.data("ok");
    }

    @GetMapping("/findById")
    @ApiOperation(value = "订单管理-查询订单", notes = "查询订单接口")
    @ApiImplicitParam(value = "订单管理-查询订单", paramType = "json")
    public Result<OrderVO> findById(@EnableAutoSubject AutoSubject subject, @RequestParam Long id) {
        log.info("mobile: {} token:{}", subject.getMobile(), subject.getToken());
        OrderVO orderVO = orderService.findById(id);
        return null == orderVO ? Result.fail("无数据") : Result.data(orderVO);
    }

    @GetMapping("/findOrderProductById")
    @ApiOperation(value = "订单管理-查询订单含产品名称", notes = "查询订单含产品名称")
    @ApiImplicitParam(value = "订单管理-查询订单含产品名称", paramType = "json")
    public Result<OrderProductVO> findOrderProductById(@EnableAutoSubject AutoSubject subject, @RequestParam Long id) {
        log.info("mobile: {} token:{}", subject.getMobile(), subject.getToken());
        OrderVO orderVO = orderService.findById(id);
        Map<String, String> headers = ImmutableMap.of(HeaderEnum.AuthToken.getValue(), subject.getToken());
        String productUri = "product/" + orderVO.getProductId();
        Instance gateWayInstance = NacosUtil.getRandomInstance(Constant.AUTO_GATEWAY_SERVER_NAME);
        String fullPath = PathUtil.getPath(RequestHolder.getHttpServletRequest().getScheme(), gateWayInstance, Constant.AUTO_PRODUCT_SERVER_NAME, productUri);
        String productDetail = restUtil.exchange(fullPath, HttpMethod.GET, headers, null, String.class);
        JSONObject jsonObject = JSONUtil.parseObj(productDetail);
        OrderProductVO orderProductVO = $.copy(orderVO, OrderProductVO.class);
        orderProductVO.setProductName(jsonObject.getStr("name"));
        return Result.data(orderProductVO);
    }


    @GetMapping("/greet")
    @ApiOperation(value = "订单管理-Openfeign调用", notes = "Greeting")
    @ApiImplicitParam(value = "订单管理-Greeting", paramType = "form")
    public Result<?> greet(@RequestParam String name) {
        String resp = productProvider.greet(name);
        Result result = JSONUtil.toBean(resp, Result.class);
        return result.isSuccess() ? Result.data(result.getData()) : Result.fail(result.getCode(), result.getMsg());
    }

}
