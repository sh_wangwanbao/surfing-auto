package com.talang.surfing.auto.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.talang.surfing.auto.order.mapper.OrderMapper;
import com.talang.surfing.auto.order.qo.OrderSaveQO;
import com.talang.surfing.auto.order.service.IOrderService;
import com.talang.surfing.auto.order.vo.OrderVO;
import com.talang.surfing.auto.order.entity.Order;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tdh.zeus.core.common.util.$;
import com.tdh.zeus.core.id.generator.wroker.IdWorkerUtil;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-23
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {
    
    @Override
    public boolean saveOrder(OrderSaveQO orderSaveQO) {
        Order order = new Order();
        order.setOrderNo(IdWorkerUtil.nextId());
        order.setUserId(orderSaveQO.getUserId());
        order.setTotalAmount(new BigDecimal(orderSaveQO.getTotalAmount()));
        order.setProductId(orderSaveQO.getProductId());
        order.setOrderStatus(0);
        return this.save(order);
    }

    @Override
    public OrderVO findById(Long id) {
        Order order = this.getOne(new LambdaQueryWrapper<Order>().eq(Order::getId, id));
        if(null != order) {
            return $.copyProperties(order, OrderVO.class);
        }
        return null;
    }
}
