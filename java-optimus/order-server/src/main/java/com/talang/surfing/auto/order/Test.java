package com.talang.surfing.auto.order;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;

import java.util.List;
import java.util.Properties;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/25 2:36 PM
 */
public class Test {
    public static void main(String[] args) {
        String serverAddr = "127.0.0.1:8848"; // 替换为您的Nacos服务器地址
        String namespace = "public"; // 替换为您的命名空间

        Properties properties = new Properties();
        properties.setProperty("serverAddr", serverAddr);
        properties.setProperty("namespace", namespace);

        try {
            // 创建NamingService实例
            NamingService namingService = NacosFactory.createNamingService(properties);

            // 获取所有服务名称
            List<String> serviceNames = namingService.getServicesOfServer(1, 10).getData();

            // 遍历服务名称并获取实例
            for (String serviceName : serviceNames) {
                List<Instance> instances = namingService.getAllInstances(serviceName);
                System.out.println("服务名称: " + serviceName);
                for (Instance instance : instances) {
                    System.out.println("实例: " + instance);
                }
            }
        } catch (NacosException e) {
            e.printStackTrace();
        }
    }
}
