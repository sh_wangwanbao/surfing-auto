package com.talang.surfing.auto.order.mapper;

import com.talang.surfing.auto.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-23
 */
public interface OrderMapper extends BaseMapper<Order> {

}
