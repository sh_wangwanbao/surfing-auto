package com.talang.surfing.auto.order.service;

import com.talang.surfing.auto.order.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import com.talang.surfing.auto.order.qo.OrderSaveQO;
import com.talang.surfing.auto.order.vo.OrderVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangwanbao
 * @since 2023-04-23
 */
public interface IOrderService extends IService<Order> {

    boolean saveOrder(OrderSaveQO orderSaveQO);
    
    OrderVO findById(Long id);
    
}
