package com.talang.surfing.auto.order.qo;

import com.tdh.zeus.core.validate.annations.ZeusValidate;
import com.tdh.zeus.core.validate.enums.Check;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/23 3:50 PM
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "OrderSaveQO", description = "创建订单信息")
public class OrderSaveQO implements Serializable {

    @ApiModelProperty("用户id")
    @ZeusValidate(required = true, fun = Check.NotEmpty)
    private Long userId;

    @ApiModelProperty("订单金额")
    @ZeusValidate(required = true, fun = Check.Number)
    private String totalAmount;

    @ApiModelProperty("产品id")
    @ZeusValidate(required = true, fun = Check.NotEmpty)
    private Long productId;
    
}
