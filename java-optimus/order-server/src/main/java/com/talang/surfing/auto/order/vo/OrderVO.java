package com.talang.surfing.auto.order.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/24 5:19 PM
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "OrderVO", description = "订单查询")
public class OrderVO implements Serializable {
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("订单单号")
    private Long orderNo;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("订单金额")
    private BigDecimal totalAmount;

    @ApiModelProperty("产品id")
    private Long productId;

    @ApiModelProperty("订单状态")
    private Integer orderStatus;

    @ApiModelProperty("创建人")
    private Long createBy;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新人")
    private Long updateBy;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("是否删除")
    private Integer isDeleted;

    @ApiModelProperty("版本")
    private Integer version;

}
