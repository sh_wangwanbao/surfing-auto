package com.talang.surfing.auto.order.provider;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/23 6:23 PM
 */
@FeignClient(name = "product-server")
public interface IProductProvider {
    
    @GetMapping("/greet")
    String greet(@RequestParam(value = "name") String name);
    
}
