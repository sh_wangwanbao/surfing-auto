package com.talang.surfing.auto.order;

import com.tdh.zeus.core.delay.EnableRedissonFastDelayQueue;
import com.tdh.zeus.core.feign.annotation.EnableMateFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/23 2:20 PM
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableMateFeign
@ComponentScans(value = {
        @ComponentScan(value = "com.tdh")
})
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableRedissonFastDelayQueue(partition = 1, poll = 1)
public class OptimusOrderServer {
    public static void main(String[] args) {
        SpringApplication.run(OptimusOrderServer.class, args);
    }
}
