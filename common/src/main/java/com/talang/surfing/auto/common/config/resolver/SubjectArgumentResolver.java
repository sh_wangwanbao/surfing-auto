package com.talang.surfing.auto.common.config.resolver;

import com.talang.surfing.auto.common.model.AutoSubject;
import com.talang.surfing.auto.common.annotations.EnableAutoSubject;
import com.tdh.zeus.core.common.enums.HeaderEnum;
import com.tdh.zeus.core.common.exception.ZeusException;
import com.tdh.zeus.core.common.util.$;
import com.tdh.zeus.core.common.util.JwtUtil;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/25 10:17 AM
 */
public class SubjectArgumentResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        boolean isEnableSubject = parameter.hasParameterAnnotation(EnableAutoSubject.class);
        boolean isAutoSubject = parameter.getParameterType().isAssignableFrom(AutoSubject.class);
        return isEnableSubject & isAutoSubject;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String jwtToken = webRequest.getHeader(HeaderEnum.AuthToken.getValue());
        if ($.isBlank(jwtToken)) {
            throw new ZeusException(401, "请登录。");
        }
        AutoSubject autoSubject = new AutoSubject();
        autoSubject.setMobile(JwtUtil.parseToken(jwtToken, String.class));
        autoSubject.setToken(jwtToken);
        return autoSubject;
    }
}
