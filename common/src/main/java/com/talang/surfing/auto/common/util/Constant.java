package com.talang.surfing.auto.common.util;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/25 2:32 PM
 */
public class Constant {
    
    //网关服务名称
    public static final String AUTO_GATEWAY_SERVER_NAME = "auto-gateway";
    
    //产品服务
    public static final String AUTO_PRODUCT_SERVER_NAME = "product-server";
    
    
    
    
}
