package com.talang.surfing.auto.common.util;

import com.alibaba.nacos.api.naming.pojo.Instance;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/25 4:26 PM
 */
public class PathUtil {
    /**
     * 拼接访问的uri
     *
     * @param protocol
     * @param instance
     * @param serverName
     * @param requestUri
     * @return
     */
    public static String getPath(String protocol, Instance instance, String serverName, String requestUri) {
        String fullUri = protocol + "://" + String.format("%s:%s", instance.getIp(), instance.getPort()) + "/" + serverName + "/" + requestUri;
        return fullUri;
    }
}
