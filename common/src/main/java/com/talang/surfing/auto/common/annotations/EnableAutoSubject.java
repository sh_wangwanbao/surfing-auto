package com.talang.surfing.auto.common.annotations;

import java.lang.annotation.*;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/25 10:07 AM
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableAutoSubject {
    
}
