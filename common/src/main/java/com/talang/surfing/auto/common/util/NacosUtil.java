package com.talang.surfing.auto.common.util;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.tdh.zeus.core.common.util.spring.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/25 2:43 PM
 */
@Slf4j
public class NacosUtil {

    private static Map<String, List<Instance>> serverNodes = Maps.newHashMap();

    public static Instance getRandomInstance(String namespace) {
        List<Instance> instances = getServerInstance(namespace);
        checkNotNull(instances);
        return Iterables.get(instances, new Random().nextInt(instances.size()));
    }

    /**
     * 从nacos中获取服务实例
     * @param serverName
     * @return
     */
    private static List<Instance> getServerInstance(String serverName) {

        if (serverNodes.containsKey(serverName)) {
            return serverNodes.get(serverName);
        }
        NacosDiscoveryProperties nacosDiscoveryProperties = SpringContextHolder.getBean(NacosDiscoveryProperties.class);
        log.info("nacosProperties:{}", nacosDiscoveryProperties);
        try {
            // 创建NamingService实例
            NamingService namingService = NacosFactory.createNamingService(nacosDiscoveryProperties.getNacosProperties());

            // 获取所有服务名称
            List<String> serviceNames = namingService.getServicesOfServer(1, 10).getData();

            // 遍历服务名称并获取实例
            for (String serviceName : serviceNames) {
                List<Instance> instances = namingService.getAllInstances(serviceName);
                if (serviceName.equals(serverName)) {
                    serverNodes.put(serverName, instances);
                    return instances;
                }
            }
        } catch (NacosException e) {
            e.printStackTrace();
        }
        return null;
    }
}
