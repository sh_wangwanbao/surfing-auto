package com.talang.surfing.auto.common.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/24 6:31 PM
 */
@ApiModel(value="user对象",description="用户对象user")
@Data
public class AutoSubject {
    
    private String mobile;
    
    private String token;
    
}
