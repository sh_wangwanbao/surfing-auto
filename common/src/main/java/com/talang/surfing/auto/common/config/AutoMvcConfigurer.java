package com.talang.surfing.auto.common.config;

import com.talang.surfing.auto.common.config.resolver.SubjectArgumentResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @Description:
 * @author: wangwanbao
 * @Date: 2023/4/25 10:35 AM
 */
@Slf4j
@Configuration
public class AutoMvcConfigurer implements WebMvcConfigurer {
    /**
     * Token参数解析
     * @param argumentResolvers 解析类
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        //注入用户信息
        argumentResolvers.add(new SubjectArgumentResolver());
    }
}
